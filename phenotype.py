# Author: Marco Esposito
# Last modified: 27/03/2022

# Methods to compute phenotypes


from typing import List, Dict, Any, Callable

from util import round_nearest, stable_hash
import numpy as np


def build_phenotype_function(parameters_info: Dict[str, Dict[str, Any]],
                             phenotype_parameters: List[str],
                             **kwargs) \
        -> Callable[[Dict[str, float], Dict[str, List[np.array]], Dict[str, Any]], str]:
    assert "tumour_sampling_period" in kwargs
    tumour_sampling_period = kwargs["tumour_sampling_period"]

    def phenotype(vp: Dict[str, float], behaviour: Dict[str, List[np.array]], level: Dict[str, Any]) -> str:
        assert "tumour_diameter" in behaviour
        # Get the trajectory of the tumour diameter with all the reference therapies
        tumour_trajectories: List[np.array] = behaviour["tumour_diameter"]
        # Compute the indices of the values of the tumour diameter at times k*tumour_sampling_period
        indices = range(0, len(tumour_trajectories[0]), tumour_sampling_period)
        # Build the trajectory of the sampled tumour diameter for each reference therapy
        sampled_tumour_diameters: List[np.array] = []
        for tumour_diameter in tumour_trajectories:
            sampled_tumour_diameters.append(np.array(tumour_diameter).take(indices))

        # Round the tumour diameter values according to the rounding value defined in the population level
        vp_phenotype: np.array = np.array([])
        tumour_diameter_round = level["tumour_diameter"]
        for sampled_tumour_diameter in sampled_tumour_diameters:
            # sizes += str(tumour_diameter // tumour_diameter_round)
            if tumour_diameter_round != 0:
                rounded_diameter = sampled_tumour_diameter // tumour_diameter_round
            else:
                rounded_diameter = sampled_tumour_diameter
            vp_phenotype = np.append(vp_phenotype, rounded_diameter)

        # Append to the phenotype the value of the parameters that are part of the phenotype (i.e. that are measurable)
        for param_id in phenotype_parameters:
            # Round the value according to the given population level
            value = round_nearest(vp[param_id], level[param_id])
            # Clip the value between its bounds
            value = min(max(value, parameters_info[param_id]["min"]), parameters_info[param_id]["max"])

            vp_phenotype = np.append(vp_phenotype, value)
        # Return the phenotype as the hash of the phenotype array
        return stable_hash(vp_phenotype)
    return phenotype


'''def phenotype(phenotype_parameters: List[str], level: dict, parameters_info,
              vp: Dict[str, float], history: np.array) -> str:
    """

    :param phenotype_parameters:
    :param level:
    :param parameters_info:
    :param vp:
    :param history:
    :return:
    """
    # sig = ""
    values = np.array(history, copy=True)
    for param_id in phenotype_parameters:
        value = round_nearest(vp[param_id], level[param_id])
        value = min(max(value, parameters_info[param_id]["min"]), parameters_info[param_id]["max"])
        # sig = f"{sig};{param_id}:{value}"
        values = np.append(values, value)
    # sig = f"{sig};{history}"
    # return stable_string_hash(sig)
    return stable_hash(values)
    # return sig



def tumour_history(tumour_sampled_trajectories: List[np.array], level: Dict[str, Any]) -> np.array:
    sizes: np.array = np.array([])
    tumour_diameter_round = level["tumour_diameter"]
    for tumour_diameter in tumour_sampled_trajectories:
        # sizes += str(tumour_diameter // tumour_diameter_round)
        if tumour_diameter_round != 0:
            abstract_diameter = tumour_diameter // tumour_diameter_round
        else:
            abstract_diameter = tumour_diameter
        sizes = np.append(sizes, abstract_diameter)
    # return ",".join(sizes)
    return sizes
    '''
