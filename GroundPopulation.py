# Author: Marco Esposito
# Created: 26/03/2022
# Last modified: 26/03/2022

from typing import Dict, Any, List, Set, Callable, Tuple
import pandas as pd
import numpy as np

from Population import Population


class GroundPopulation(Population):

    def __init__(self, level_index, level_id, level_params, dump_filename_template,
                 phenotype_function: Callable[[Dict[str, float], Dict[str, List[np.array]]], str],
                 hash_function: Callable[[Dict[str, float]], str]):
        super(GroundPopulation, self).__init__(level_index, level_id, level_params, dump_filename_template,
                                               phenotype_function)
        self.virtual_patients: pd.DataFrame = pd.DataFrame([])
        self.hashes: Set[str] = set()
        self.updated: bool = True
        self.new_vps_buffer: List[Dict[str, Any]] = []
        self.hash_function: Callable[[Dict[str, float]], str] = hash_function

    '''def add_virtual_patients(self,
                             vps_with_behaviours: List[Tuple[Dict[str, float], Dict[str, List[np.array]]]]) -> None:
        if len(vps_with_behaviours) == 0:
            return
        for vp, behaviour in vps_with_behaviours:
            assert "phenotype" not in vp
            # Adds the phenotype and the vp to the buffer
            added_phenotype = super(GroundPopulation, self).add_phenotype(vp, behaviour)
            added_hash = self.hashes.add(self.hash_function(vp))
            assert added_hash == added_phenotype
            if added_phenotype and added_hash:
                self.updated = False
                self.new_vps_buffer += [vp]'''

    def add_virtual_patient(self, vp: Dict[str, Any]) -> bool:
        assert "phenotype" not in vp
        vp_hash = self.hash_function(vp)
        if vp_hash in self.hashes:
            return False

        self.hashes.add(vp_hash)
        self.updated = False
        self.new_vps_buffer += [vp]
        return True

    def add_virtual_patient_with_hash(self, vp: Dict[str, Any], vp_hash: str) -> bool:
        assert "phenotype" not in vp

        if vp_hash in self.hashes:
            return False

        self.hashes.add(vp_hash)
        self.updated = False
        self.new_vps_buffer += [vp]
        return True

    def has_virtual_patient(self, vp: Dict[str, Any]) -> bool:
        return self.hash_function(vp) in self.hashes

    def has_virtual_patient_by_hash(self, vp_hash: str) -> bool:
        return vp_hash in self.hashes

    def compute_hash(self, vp: Dict[str, Any]) -> str:
        return self.hash_function(vp)

    def sample_virtual_patient(self) -> Dict[str, float]:
        if self.is_empty():
            return dict()
        return self.virtual_patients.sample().to_dict(orient='records')[0]

    def update_sampling_policy(self) -> bool:
        """
        Adds all virtual patients currently in self.new_vps_buffer to self.virtual_patients and empties the buffer
        :return: True if there were buffered VPs and False otherwise
        """
        assert self.updated == (len(self.new_vps_buffer) == 0)

        if len(self.new_vps_buffer) == 0:
            # No virtual patient has been discovered since the last sampling policy update
            self.updated = True     # redundant, see previous assertion
            return False

        buffered_new_vps_df: pd.DataFrame = pd.DataFrame(self.new_vps_buffer,
                                                         index=[k for k in
                                                                range(self.virtual_patients.shape[0],
                                                                      # index of the first vp to append
                                                                      self.virtual_patients.shape[0] + len(
                                                                          self.new_vps_buffer))])  # index of last vp
        self.virtual_patients = pd.concat([self.virtual_patients, buffered_new_vps_df])
        # All new virtual patients have been added to the stable population, so the buffer can be emptied
        self.new_vps_buffer = []
        self.updated = True
        return True

    def load(self, filename_template: str) -> None:
        phenotypes_df: pd.DataFrame = pd.read_csv(filename_template.format(self.level_index, self.level_id), sep="\t")
        self.phenotypes = set([s for s in phenotypes_df["phenotype"]])
        for vp in phenotypes_df.to_dict(orient='records'):
            self.add_virtual_patient({p: v for p, v in vp.items() if p != "phenotype"})
