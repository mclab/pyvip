# Author: Marco Esposito
# Last modified: 24/03/2022

# Utility methods
import hashlib
import math
import random
from typing import List, Dict, Any, Callable, Hashable, Set


def round_nearest(x, a):
    if a == 0:
        return x
    return round(x / a) * a


def round_nearest_int(x, a) -> int:
    return int(x // a)


def power_law(x, a=-1.01):
    return x ** a


def stable_string_hash(s: str, hashfun=hashlib.md5) -> str:
    return hashfun(s.encode('UTF-8')).hexdigest()


def stable_hash(s: Any, hashfun=hashlib.md5) -> str:
    return hashfun(s).hexdigest()


def get_vp_hash_function(parameters_info: Dict[str, Dict[str, Any]]) -> Callable[[Dict[str, Any]], str]:
    sorted_parameters_ids = list(parameters_info.keys())
    sorted_parameters_ids.sort()

    def vp_hash(vp: Dict[str, Any]) -> str:
        sig = ""
        for param_id in sorted_parameters_ids:
            if parameters_info[param_id]["type"] == "bounds":
                if parameters_info[param_id]["step"] == 1:
                    value = int(vp[param_id])
                else:
                    value = round_nearest_int(vp[param_id], parameters_info[param_id]['step'])
                sig = f"{sig};{param_id}:{value}"
            else:
                sig = f"{sig};{param_id}:{vp[param_id]}"
        return stable_string_hash(sig)
    return vp_hash


def sample_num_of_parameters_to_change(a, max_value):
    while True:
        sample = math.floor(power_law(random.random(), a))
        if sample <= max_value:
            return sample


def get_random_param_value(param_info: Dict[str, any], exclude=None, decimal_places=2) -> float:
    while True:
        if "values" in param_info:
            values = param_info["values"]
            value = random.choice(values)
        else:  # type="bounds"
            value = param_info["min"] + random.random() * (param_info["max"] - param_info["min"])
            if param_info["step"] == 1:
                value = int(value)
            else:
                value = int(round_nearest(value, param_info['step'])*(10**decimal_places))/(10**decimal_places)

        if exclude is None or value != exclude:
            return value


def index_repeated_elements(elements: List[Hashable]) -> Set[int]:
    values: Set = set()
    repeated: Set[int] = set()
    for index, element in enumerate(elements):
        if element not in values:
            values.add(element)
        else:
            repeated.add(index)
    return repeated
