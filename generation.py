# Author: Marco Esposito
# Last modified: 26/03/2022

import json
import math
import os
import random
import sys
from typing import *

from AdmissibilityChecker import AdmissibilityChecker
from multiprocessing import Lock, Manager, Pool

import numpy as np

from GroundPopulation import GroundPopulation
from Population import Population
from phenotype import build_phenotype_function
from util import get_random_param_value, sample_num_of_parameters_to_change, get_vp_hash_function, \
    index_repeated_elements

checker: AdmissibilityChecker = None
lock: Lock = Lock()
taken: Set[AdmissibilityChecker] = set()


def run_generation(model_path: str, checker_class: ClassVar,
                   parameters: Dict[str, Dict[str, Any]],
                   levels: List[Dict[str, Any]],
                   delta: float, epsilon: float,
                   policy_revision_delta: float, policy_revision_epsilon: float,
                   n_workers: int,
                   samples_per_iteration: int,
                   dump_filename_template: str,
                   phenotype_parameters: List[str],
                   initial_failures: Dict[int, int],
                   failures_dump_filename: str,
                   max_failures_dump_filename: str,
                   tumour_sampling_period: int,
                   given_vps_filename_template: str = None):
    # Build the VP hash function
    vp_hash_function: Callable = get_vp_hash_function(parameters)

    # Build the phenotype function, that given a VP, its behaviour and a level, computes its phenotype for that level
    phenotype_function: Callable[[Dict[str, float], Dict[str, List[np.array]], Dict[str, Any]], str] = \
        build_phenotype_function(parameters, phenotype_parameters,
                                 tumour_sampling_period=tumour_sampling_period)

    # Initialize checkers: objects responsible for checking if candidate VPs are admissible, via simulation
    checkers: List[AdmissibilityChecker] = [checker_class(model_path) for _ in range(n_workers)]

    # Populations of VPs, one for each level
    populations: List[Population] = []

    # Build "ground" population: the population at the lowest level, which holds all the admissible VPs found.
    # Two VPs have the same phenotype for this population iff they have the same values for all parameters
    knownVPs = GroundPopulation(0, levels[0]["id"], levels[0], dump_filename_template,
                                phenotype_function, vp_hash_function)
    populations.append(knownVPs)

    # Initialize the populations for every other layer
    for level_idx in range(1, len(levels)):
        level = levels[level_idx]
        populations.append(Population(level_idx, level["id"], level, dump_filename_template, phenotype_function))

    # Load existing populations, if any
    if given_vps_filename_template is not None:
        for level_idx, level in enumerate(levels):
            populations[level_idx].load(given_vps_filename_template.format(level_idx, level["id"]))
    else:
        # Bootstrap: sample from the parameter space until an admissible VP is found
        initial_vp, init_time, behaviour = generate_first_VP(checkers[0], parameters)
        initial_vp["init_time"] = init_time
        knownVPs.add_virtual_patient(initial_vp)
        print(f"Initial VP {initial_vp} with hash {knownVPs.compute_hash(initial_vp)} added to knownVPs")
        for population in populations:
            population.add_phenotype(initial_vp, behaviour)

    for population in populations:
        population.dump(append=False)

    # Make sure that subsequent calls to knownVPs.sample_virtual_patients() will sample from all known virtual patients
    knownVPs.update_sampling_policy()

    search_virtual_patients(checkers,
                            parameters,
                            knownVPs,
                            populations,
                            delta, epsilon,
                            policy_revision_delta, policy_revision_epsilon,
                            n_workers,
                            samples_per_iteration,
                            initial_failures,
                            failures_dump_filename,
                            max_failures_dump_filename,
                            a=-1.01)


def generate_first_VP(crc_checker: AdmissibilityChecker, parameters: Dict[str, Any]) -> \
        Tuple[Dict[str, Any], int, Dict[str, List[np.array]]]:
    attempt = 1
    print("Generating first Virtual Patient", flush=True)
    while True:
        print(f"Attempt {attempt}")
        attempt += 1
        candidate: Dict[str, float] = dict()

        for param_id, param_info in parameters.items():
            candidate[param_id] = get_random_param_value(param_info)
        is_adm, init_time, trajectories = crc_checker.is_admissible(candidate)

        if is_adm:
            print("\nDone!", flush=True)
            return candidate, init_time, trajectories


def init_process(crc_checkers: List[AdmissibilityChecker]):
    global checker
    with lock:
        print('Initializing checker on process => {}'.format(os.getpid()), flush=True)

        for c in crc_checkers:
            if c not in taken:
                checker = c
                taken.add(c)


def check_admissibility_worker(candidate: Dict[str, float], index, results):
    is_adm, init_time, behaviour = checker.is_admissible(candidate)
    results[index] = (is_adm, init_time, behaviour)


def search_virtual_patients(checkers: List[AdmissibilityChecker],
                            parameters: Dict[str, Dict[str, Any]],
                            knownVPs: GroundPopulation,
                            populations: List[Population],
                            delta: float, epsilon: float,
                            policy_revision_delta: float, policy_revision_epsilon: float,
                            n_cores: int, samples_per_iteration: int,
                            initial_failures: Dict[int, int],
                            failures_dump_filename: str,
                            max_failures_dump_filename: str,
                            a=-1.01) -> None:
    # MC^2 failures
    failures: Dict[int, int] = dict(initial_failures)
    # Target number of failures
    M: int = int(math.ceil(math.log(delta) / math.log(1 - epsilon)))
    # Maximum number of failures reached for each level
    max_failures: Dict[int, int] = dict(initial_failures)
    # Number of samples from known virtual patients between sampling policy revisions
    policy_revision_samples: int = int(math.ceil(math.log(policy_revision_delta) /
                                                 math.log(1 - policy_revision_epsilon)))

    # Initialize multiprocessing manager (we just use manager.dict() for collecting starmap() results)
    manager = Manager()

    policy_samples: int = 0  # current number of samples with the current sampling policy
    completed_levels: Set[int] = set()  # levels for which the target number of failures has been reached
    # Initialize a pool of n_workers processes, each initialized with the function init_process(...).
    # The pool stays active (the processes are not terminated) until the closure of the with,
    # i.e. the completion of all levels
    with Pool(processes=n_cores, initializer=init_process, initargs=(checkers,)) as pool:
        while len(completed_levels) < len(failures.keys()):
            results = manager.dict()
            print("Current failures: " + str({i: f for i, f in failures.items() if i not in completed_levels}) + "\n",
                  flush=True)
            print("Max consecutive failures: " + str({i: f for i, f in max_failures.items()
                                                      if i not in completed_levels}) + "\n", flush=True)

            # The candidates are extracted from "ground" VPs, i.e. VPs added after the last sampling policy revision
            candidates = sample_candidates(parameters, knownVPs, samples_per_iteration, a)
            # Update the number of samples taken from this population
            policy_samples += samples_per_iteration

            # Determine which candidates can already be considered failures and which ones must be simulated

            # First, compute the hash of all candidates
            candidates_hashes: List[str] = [knownVPs.compute_hash(candidates[i]) for i in range(len(candidates))]

            # 1. Sampling a candidate that has already been seen is considered a failure
            resampled_indices: Set[int] = {i for i in range(len(candidates))
                                           if knownVPs.has_virtual_patient_by_hash(candidates_hashes[i])}

            # 2. For all candidates that are sampled more than once in this batch, all but the first copy are failures
            resampled_indices.update(index_repeated_elements(candidates_hashes))
            print(f"Number of resampled candidates: {len(resampled_indices)}")

            # Build the list of admissibility checking tasks, excluding all candidates that represent a failure
            admissibility_tasks = [(candidates[i], i, results) for i in range(len(candidates))
                                   if i not in resampled_indices]

            print(f"Simulating all {len(admissibility_tasks)} new candidates...", flush=True)
            # Run all admissibility tasks in parallel on the pool
            pool.starmap(check_admissibility_worker, admissibility_tasks)
            print(f"Simulations batch completed", flush=True)

            # Dictionaries for logging batch results
            n_resampled = {i: 0 for i in range(len(populations)) if i not in completed_levels}
            n_new = {i: 0 for i in range(len(populations)) if i not in completed_levels}
            n_already_found = {i: 0 for i in range(len(populations)) if i not in completed_levels}
            n_not_adm = {i: 0 for i in range(len(populations)) if i not in completed_levels}

            print("Processing batch results...", flush=True)

            for cand_idx in range(len(candidates)):
                is_admissible = False
                for level_idx, population in enumerate(populations):
                    if level_idx in completed_levels:
                        continue
                    if cand_idx in resampled_indices:
                        n_resampled[level_idx] += 1
                        failure = True
                    else:  # new candidate
                        is_admissible, init_time, behaviour = results[cand_idx]
                        if is_admissible:
                            candidates[cand_idx]["init_time"] = init_time
                            added = population.add_phenotype(candidates[cand_idx], behaviour)
                            if added:  # the phenotype was not already present in the population
                                n_new[level_idx] += 1
                                failure = False
                            else:  # the phenotype was already present in the population
                                n_already_found[level_idx] += 1
                                failure = True
                        else:  # candidate not admissible
                            n_not_adm[level_idx] += 1
                            failure = True

                    if failure:  # candidate resampled, not admissible or already found
                        failures[level_idx] += 1
                        max_failures[level_idx] = max(max_failures[level_idx], failures[level_idx])
                        if failures[level_idx] >= M:
                            print(f"\n\n===============\nLevel {level_idx} "
                                  f"reached {M} failures => completed.\n===============\n\n ", flush=True)
                            completed_levels.add(level_idx)
                    else:
                        failures[level_idx] = 0
                # If the candidate is admissible, add it to the set of ground Virtual Patients
                if is_admissible:
                    added = knownVPs.add_virtual_patient_with_hash(candidates[cand_idx],
                                                                   candidates_hashes[cand_idx])
                    assert added, f"Previously unseen VP {candidates[cand_idx]} with hash" \
                                  f" {candidates_hashes[cand_idx]} not added to knownVPs"

            print("Done processing batch results.", flush=True)

            print("Dumping populations...", flush=True)
            for population in populations:
                population.dump()

            with open(failures_dump_filename, "w") as failure_file:
                json.dump(failures, failure_file, indent=4)

            with open(max_failures_dump_filename, "w") as failure_file:
                json.dump(failures, failure_file, indent=4)

            total_vps: Dict[str, int] = {li: populations[li].total_VPs() for li in range(len(populations))}

            print(f"New VPs: {n_new}", flush=True)
            print(f"Resampled candidates: {n_resampled}", flush=True)
            print(f"VPs already found: {n_already_found}", flush=True)
            print(f"Non-admissible candidates: {n_not_adm}\n", flush=True)
            print(f"Total VPs found so far: {total_vps}\n", flush=True)

            print("Done dumping populations.", flush=True)

            print("\n====================== END BATCH ======================\n")

            # Policy revision
            if policy_samples >= policy_revision_samples:
                print(f"Reached {policy_samples} >= {policy_revision_samples} samples. Revision the sampling policy",
                      flush=True)
                failures = {i: 0 for i in range(len(populations))}
                knownVPs.update_sampling_policy()
                policy_samples = 0
                print("\n============= END SAMPLE POLICY REVISION =============\n")


def sample_candidates(parameters: Dict[str, Dict[str, Any]], knownVPs: GroundPopulation, n_candidates: int, a) \
        -> List[Dict[str, Any]]:
    candidates: List[Dict[str, Any]] = []
    while len(candidates) < n_candidates:
        vp: Dict[str, float] = knownVPs.sample_virtual_patient()
        n_changes = sample_num_of_parameters_to_change(a, len(parameters))
        params_to_change = random.sample(parameters.keys(), n_changes)
        c: Dict[str, float] = dict()
        for param_id, param_info in parameters.items():
            if param_id in params_to_change:
                c[param_id] = get_random_param_value(param_info, exclude=vp[param_id])
            else:
                c[param_id] = vp[param_id]
        candidates += [c]
    return candidates
