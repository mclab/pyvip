#!/bin/bash

echo ${_CONDOR_JOB_IWD}

python3 /generation/pyvip/main.py \
  -m /generation/pyvip/crc/data/model/CRC_treatment_L3V1_generation_Bioinformatics.xml \
  -p /generation/pyvip/crc/data/slices/slice1.json \
  -l /generation/pyvip/crc/data/levels.json  \
  -f f/population_dump_{}_{}.tsv \
  --dump_failures_filename ${_CONDOR_JOB_IWD}/failures.json \
  -c 64 -n 2995 -e 0.001 -d 0.05
