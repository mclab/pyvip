# Author: Marco Esposito
# Created: 26/03/2022
# Last modified: 26/03/2022

from typing import Dict, Any, List, Set, Callable
import pandas as pd
import numpy as np


class Population:

    def __init__(self, level_index, level_id, level_params, dump_filename_template,
                 phenotype_function: Callable[[Dict[str, float], Dict[str, List[np.array]], Dict[str, Any]], str]):
        self.level_index: int = level_index
        self.level_id: str = level_id
        self.level_params: Dict[str, Any] = level_params
        self.dump_filename: str = dump_filename_template.format(level_index, level_id)
        self.phenotypes: Set[str] = set()
        self.phenotype_function: Callable[[Dict[str, float], Dict[str, List[np.array]]], str] = phenotype_function

        # The buffer holds virtual patients that are added, until dump() is called.
        # At that point, self.virtual_patients is updated with the VPs in the buffer and the whole dataframe is saved.
        self.vp_buffer: List[Dict[str, float]] = []

    def add_phenotype(self, vp: Dict[str, Any], behaviour: Dict[str, List[np.array]]) -> bool:
        phenotype: str = self.phenotype_function(vp, behaviour, self.level_params)
        if self.has_phenotype(phenotype):
            return False
        self.phenotypes.add(phenotype)
        temp_vp = dict(vp)
        temp_vp["phenotype"] = phenotype
        self.vp_buffer += [temp_vp]
        return True

    def has_phenotype(self, phenotype: str) -> bool:
        return phenotype in self.phenotypes

    def compute_phenotype(self, vp: Dict[str, Any], behaviour: Dict[str, List[np.array]]) -> str:
        return self.phenotype_function(vp, behaviour)

    def is_empty(self) -> bool:
        return len(self.phenotypes) == 0

    def has_vps_in_buffer(self) -> bool:
        return len(self.vp_buffer) > 0

    def dump(self, sep: str = "\t", index: bool = False, append: bool = True) -> bool:
        if not self.has_vps_in_buffer():
            return False
        buffered_vps_df: pd.DataFrame = pd.DataFrame(self.vp_buffer,
                                                     index=[k for k in
                                                            range(len(self.phenotypes),
                                                                  # index of the first vp to append
                                                                  len(self.phenotypes) + len(
                                                                      self.vp_buffer))])  # index of last vp
        if append:
            buffered_vps_df.to_csv(self.dump_filename, sep=sep, mode='a', header=False, index=index)
        else:
            buffered_vps_df.to_csv(self.dump_filename, sep=sep, index=index)
        # All buffered virtual patients have been saved to file, the buffer can be emptied
        self.vp_buffer = []
        return True

    def load(self, filename_template: str) -> None:
        phenotypes_df = pd.read_csv(filename_template.format(self.level_index, self.level_id), sep="\t")
        self.phenotypes = set([s for s in phenotypes_df["phenotype"]])

    def total_VPs(self):
        return len(self.phenotypes)
