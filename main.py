# Author: Marco Esposito
# Last modified: 25/03/2022
import json
import random
from typing import Dict, Any

import numpy as np

from generation import run_generation
from cli import get_command_line_arguments
from parameters import build_parameters

cli_args = get_command_line_arguments()

model_path = cli_args["model"]  # data/model/CRC_treatment_L3V1_generation_Bioinformatics.xml"  # path to model
parameters_filename = cli_args["parameters"]

dump_filename_without_slice = cli_args['dump_filename']
slice_name = parameters_filename.split("/")[-1].split(".")[0]

dump_prefix = "/".join(dump_filename_without_slice.split("/")[:-1])
dump_suffix = dump_filename_without_slice.split("/")[-1]

dump_filename = f"{dump_prefix}/{slice_name}_{dump_suffix}"

levels_filename = cli_args["levels_definitions"]

phenotype_parameters = ["n_T1_clones"]

epsilon = cli_args["epsilon"]
delta = cli_args["delta"]
policy_revision_delta = cli_args["policy_revision_delta"]
policy_revision_epsilon = cli_args["policy_revision_epsilon"]

n_cores = cli_args["n_cores"]

samples_per_iteration = cli_args["samples_per_iter"]

checker_class = cli_args['checker_class']

dump_failures_filename = cli_args['dump_failures_filename']

max_failures_prefix = "/".join(dump_failures_filename.split("/")[:-1])
max_failures_suffix = dump_failures_filename.split("/")[-1]
max_failures_dump_filename = f"{max_failures_prefix}/max_{max_failures_suffix}"


initial_failures_filename = cli_args['initial_failures_filename']
given_vps_filename = cli_args['given_vps_filename']
if not given_vps_filename:
    given_vps_filename = None
else:
    given_vps_prefix = "/".join(given_vps_filename.split("/")[:-1])
    given_vps_suffix = given_vps_filename.split("/")[-1]

    given_vps_filename = f"{given_vps_prefix}/{slice_name}_{given_vps_suffix}"

levels_definitions_file = open(levels_filename, "r")
levels = json.load(levels_definitions_file)
levels_definitions_file.close()

tumour_sampling_period = levels[0]["tumour_window"]

if not initial_failures_filename:
    initial_failures = {i: 0 for i in range(len(levels))}
else:
    initial_failures_file = open(initial_failures_filename, "r")
    initial_failures_str = json.load(initial_failures_file)
    initial_failures_file.close()
    initial_failures = {int(i): f for i, f in initial_failures_str.items()}

parameters: Dict[str, Dict[str, Any]] = build_parameters(parameters_filename)

random.seed(cli_args["seed"])
np.random.seed(cli_args["seed"])

run_generation(model_path, checker_class,
               parameters,
               levels,
               delta, epsilon,
               policy_revision_delta, policy_revision_epsilon,
               n_cores,
               samples_per_iteration,
               dump_filename,
               phenotype_parameters,
               initial_failures,
               dump_failures_filename,
               max_failures_dump_filename,
               tumour_sampling_period,
               given_vps_filename)
