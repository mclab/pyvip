FROM ubuntu:focal

LABEL maintainer="Marco Esposito <esposito@di.uniroma1.it>"

# Install dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    libncurses5 \
    libncurses5-dev \
    python3 \
    python3-dev \
    python3-pip \
    git \
    nano \
    && rm -rf /var/lib/apt/lists/*


RUN mkdir /generation
RUN mkdir /generation/results

WORKDIR /generation

# Clone BitBucket repository
RUN git clone https://marcoesposito_di@bitbucket.org/mclab/pyvip.git

# Install python dependencies
WORKDIR /generation/pyvip

# RUN python3 -m venv env

# RUN source env/bin/activate

RUN pip3 install apricopt==0.0.2a3dev8 codetiming

CMD ["bin/bash"]