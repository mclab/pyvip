import json
from typing import List, Dict, Any, Tuple


def build_parameters(parameters_file: str) -> Dict[str, Dict[str, Any]]:
    params_file = open(parameters_file, "r")
    parameters = json.load(params_file)
    params_file.close()
    return parameters
