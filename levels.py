# Un livello è definito da

# - discretizzazione del diametro del tumore
#   - da

# - distanza tra due osservazioni del tumore (window)
#   -  [1, 7, 14, 28, 56, 100, 200, 400]

# - discretizzazione del TMB
#   [1, 2, 5, 10, 20, 50]
from typing import Dict, List, Any
import json

'''
example input
phenotype_def = {
    "tumour_diameter": [0.1, 0.5, 1, 2, 5, 10],
    "tumour_window": [1, 7, 14, 28, 56, 100, 400],
    "n_T1_clones": [1, 2, 5, 10, 20, 50]
}'''


def build_levels_old(phenotype_def: Dict[str, List[float]]) -> List[Dict[str, Any]]:
    """
    THE FIRST ELEMENT IN THE RESULT MUST CORRESPOND TO THE MOST FINE-GRAINED POPULATION
    :param phenotype_def:
    :return:
    """
    result: List[Dict[str, Any]] = []
    for td in phenotype_def["tumour_diameter"]:
        for tw in phenotype_def["tumour_window"]:
            for tmb in phenotype_def["n_T1_clones"]:
                result += [{"id": f"td_{td}_tw_{tw}_tmb_{tmb}",
                            "tumour_diameter": td, "tumour_window": tw, "n_T1_clones": tmb}]
    return result


def build_levels(phenotype_def: Dict[str, List[float]], distance=2) -> List[Dict[str, Any]]:
    """
    THE FIRST ELEMENT IN THE RESULT MUST CORRESPOND TO THE MOST FINE-GRAINED POPULATION
    :param distance:
    :param phenotype_def:
    :return:
    """
    index = 0
    result: List[Dict[str, Any]] = []
    for itd, td in enumerate(phenotype_def["tumour_diameter"]):
        for itw, tw in enumerate(phenotype_def["tumour_window"]):
            for itmb, tmb in enumerate(phenotype_def["n_T1_clones"]):
                if abs(itd - itw) <= distance and abs(itd - itmb) <= distance and abs(itw - itmb) <= distance:
                    result += [{"index": index, "id": f"td_{td}_tw_{tw}_tmb_{tmb}",
                                "tumour_diameter": td, "tumour_window": tw, "n_T1_clones": tmb}]
                    index += 1

    f = open("crc/data/levels.json", "w+")
    json.dump(result, f, indent=4)
    f.close()
    return result
