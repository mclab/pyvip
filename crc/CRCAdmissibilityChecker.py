from typing import *
import math
import codetiming as ct
from apricopt.model.Model import Model
from apricopt.simulation.SimulationEngine import SimulationEngine
from apricopt.simulation.roadrunner.RoadRunnerEngine import RoadRunnerEngine

from AdmissibilityChecker import AdmissibilityChecker

import numpy as np


def is_responder_by_derivative(trajectory: Dict[str, List[float]]) -> bool:
    tumour_diameter = trajectory['tumour_diameter']
    old_variation = 0  # float("Inf")
    for t in range(1, len(tumour_diameter)):
        tum_prev = tumour_diameter[t - 1]
        tum = tumour_diameter[t]
        new_variation = tum - tum_prev
        if new_variation < old_variation:
            return True
        old_variation = new_variation
    return False


def is_responder_by_time_to_response(trajectory: Dict[str, List[float]]) -> bool:
    tumour_diameter = trajectory['tumour_diameter']
    for t in range(1, len(tumour_diameter)):
        tum_prev = tumour_diameter[t - 1]
        tum = tumour_diameter[t]
        if tum < tum_prev:
            return True
    return False


def is_responder_by_tumour_diameter(trajectory: Dict[str, List[float]]) -> bool:
    tumour_diameter = trajectory['tumour_diameter']
    initial_tumour_diameter = tumour_diameter[0]
    for t in range(1, len(tumour_diameter)):
        # tum_prev = tumour_diameter[t - 1]
        # tum = tumour_diameter[t]
        if tumour_diameter[t] < initial_tumour_diameter:
            # CHECK: this method returns the tumour diameter at the end of simulation
            return True
    return False


def is_responder(trajectory, do_check=True):
    # return is_responder_by_tumour_diameter(trajectory)
    if do_check:
        '''if not second_der:
        isresponder, reduction = is_responder_by_time_to_response(trajectory)
        return isresponder, reduction
    else:
        isresponder, variation = is_responder_by_derivative(trajectory)
        return isresponder, variation'''
        return is_responder_by_time_to_response(trajectory)
    else:
        return True


def _set_exclude_from_initialisation_element():
    exclude_from_initialisation = [
        "syn_T1_C1__PD1_PDL1",
        "syn_T1_C1__PD1_PDL2",
        "syn_T1_C1__PD1",
        "syn_T1_C1__PDL1",
        "syn_T1_C1__PDL2",
        "syn_T1_C1__PD1_aPD1",
        "syn_T1_C1__PD1_aPD1_PD1",
        "syn_T1_C1__PDL1_aPDL1",
        "syn_T1_C1__PDL1_aPDL1_PDL1",
        "syn_T1_APC__PD1_PDL1",
        "syn_T1_APC__PD1_PDL2",
        "syn_T1_APC__PD1",
        "syn_T1_APC__PDL1",
        "syn_T1_APC__PDL2",
        "syn_T1_APC__PD1_aPD1",
        "syn_T1_APC__PD1_aPD1_PD1",
        "syn_T1_APC__PDL1_aPDL1",
        "syn_T1_APC__PDL1_aPDL1_PDL1",
        "TCEsyn_T1_C1",
        "TCEsyn_T0_C1",
        "TCEsyn_T1_C1__CEA_cibis_CEA_CD3",
        "TCEsyn_T1_C1__CEA_cibis_CEA",
        "TCEsyn_T1_C1__CEA_cibis_CD3",
        "TCEsyn_T1_C1__CD3_cibis",
        "TCEsyn_T1_C1__CEA_cibis",
        "TCEsyn_T1_C1__CEA",
        "TCEsyn_T1_C1__CD3",
        "TCEsyn_T0_C1__CEA_cibis_CEA_CD3",
        "TCEsyn_T0_C1__CEA_cibis_CEA",
        "TCEsyn_T0_C1__CEA_cibis_CD3",
        "TCEsyn_T0_C1__CD3_cibis",
        "TCEsyn_T0_C1__CEA_cibis",
        "TCEsyn_T0_C1__CEA",
        "TCEsyn_T0_C1__CD3"
    ]
    return exclude_from_initialisation


def _set_transient_observed_outputs():
    observed_outputs = [
        'time',
        'admissibility_cancer_cells_tumour_monitor',
        'admissibility_H_APC_monitor',
        'admissibility_H_CEA_C1_T0_monitor',
        'admissibility_H_CEA_C1_T1_monitor',
        'admissibility_H_mAPC_monitor',
        'admissibility_H_P0_monitor',
        'admissibility_H_P1_monitor',
        'admissibility_H_PD1_APC_monitor',
        'admissibility_H_PD1_C1_monitor',
        'admissibility_teff_blood_monitor',
        'admissibility_teff_tumour_monitor',
        'admissibility_treg_blood_monitor',
        'admissibility_treg_tumour_monitor',
        'admissibility_tumour_volume_monitor',
        'initialization_tumour_diameter_monitor',
        'tumour_diameter'
    ]
    return observed_outputs


class CRCAdmissibilityChecker(AdmissibilityChecker):

    def __init__(self, model_path: str, debug=False):
        self.model_path = model_path

        self.sim_engine: SimulationEngine = RoadRunnerEngine()
        self.model = Model(model_filename=model_path, sim_engine=self.sim_engine, abs_tol=1e-12,
                           rel_tol=1e-6, time_step=1, observed_outputs=_set_transient_observed_outputs())
        self.reference_treats: List[Dict[str, float]] = []
        self.sim_timer = ct.Timer(logger=None)
        self.candidate_timer = ct.Timer(logger=None)
        self.execution_times = dict()
        self.init_exclude = _set_exclude_from_initialisation_element()
        self.load_reference_treatments()
        self.debug = debug

        self.candidate_init_time = 0

    def is_admissible(self, vp: Dict[str, float], **kwargs) -> Tuple[bool, int, Dict[str, List[np.array]]]:
        self.set_candidate_vp(vp)
        adm, init_time, behaviour = self.check_admissible()
        self.reset_generation_object()
        return adm, init_time, behaviour

    def load_reference_treatments(self) -> None:
        treatments = {
            '0': {
                'ate_dosage': 1680,
                'ate_freq_in': 4,
                'cibi_dosage': 0,
                'cibi_freq_in': 3,
            },
            '1': {
                'ate_dosage': 0,
                'ate_freq_in': 4,
                'cibi_dosage': 160,
                'cibi_freq_in': 3,
            },
            '2': {
                'ate_dosage': 1680,
                'ate_freq_in': 4,
                'cibi_dosage': 160,
                'cibi_freq_in': 3,
            }

        }
        '''
        '4': {
                'ate_dosage': 1200,
                'ate_freq_in': 4,
                'cibi_dosage': 160,
                'cibi_freq_in': 3,
            }
            '1': {
                'ate_dosage': 1200,
                'ate_freq_in': 4,
                'cibi_dosage': 0,
                'cibi_freq_in': 3,
            },'''
        self.reference_treats = [treat for _, treat in sorted(treatments.items(), key=lambda i: int(i[0]))]

    def set_candidate_vp(self, candidate: Dict[str, float]) -> None:
        self.model.set_fixed_params(candidate)

    def get_candidate_init_time(self) -> float:
        return self.candidate_init_time

    def reset_init_time(self) -> None:
        self.candidate_init_time = -1

    def check_admissible(self, check_response=True) -> Tuple[bool, int, Dict[str, List[np.array]]]:
        try:
            # observed_outputs_admissibility = self._set_transient_observed_outputs()
            # self.model.observed_outputs = observed_outputs_admissibility
            self.candidate_timer.start()

            # Simulate and check monitors for transient
            self.sim_timer.start()
            is_init, init_time, all_ref_therapy_trajectories = self.simulate_for_initialisation(8000)
            self.execution_times['simulation_initialisation'] = self.sim_timer.stop()
            if not is_init:
                self.candidate_init_time = init_time
                self.execution_times['candidate_time'] = self.candidate_timer.stop()
                return False, 0, dict()
            init_time = abs(init_time)
            self.candidate_init_time = init_time
            if not self._check_all_monitors_for_transient(all_ref_therapy_trajectories, init_time):
                self.execution_times['candidate_time'] = self.candidate_timer.stop()
                return False, 0, dict()

            # Initialisation of the candidate
            self.sim_timer.start()

            self.sim_engine.simulate_and_set(self.model, init_time, exclude=self.init_exclude,
                                             evaluate_constraints=False)
            self.execution_times['patient_initialisation'] = self.sim_timer.stop()

            # Simulation and check monitors without treatment
            self.sim_timer.start()
            is_sim, all_ref_therapy_trajectories = self.simulate_for_admissibility(400)
            self.execution_times['simulation_no_treatment'] = self.sim_timer.stop()
            if not is_sim:
                self.execution_times['candidate_time'] = self.candidate_timer.stop()
                return False, 0, dict()
            else:
                if not self._check_all_monitors_for_simulation(all_ref_therapy_trajectories, 0):
                    self.execution_times['candidate_time'] = self.candidate_timer.stop()
                    return False, 0, dict()

            # Simulation and check monitors for all reference treatments
            cumulative_time = -1
            # List of all tumour diameter trajectories, one for each reference therapy
            behaviour: List[np.array] = []

            responds = False
            for i in range(len(self.reference_treats)):
                self._set_ref_treatments(i)
                self.sim_timer.start()
                is_sim, all_ref_therapy_trajectories = self.simulate_for_admissibility(400)

                cumulative_time += self.sim_timer.stop()
                if not is_sim:
                    self.candidate_timer.stop()
                    self.execution_times['simulation_treatments_cumulative'] = cumulative_time
                    return False, 0, dict()
                else:
                    if not self._check_all_monitors_for_simulation(all_ref_therapy_trajectories, i + 1):
                        self.candidate_timer.stop()
                        self.execution_times['simulation_treatments_cumulative'] = cumulative_time
                        return False, 0, dict()

                # Patient could be simulated with reference therapy and is admissible w.r.t. it
                behaviour += [np.array(all_ref_therapy_trajectories["tumour_diameter"])]

                # Check whether the therapy caused a response
                if is_responder(all_ref_therapy_trajectories, do_check=check_response):
                    responds = True

            if not responds:
                self.candidate_timer.stop()
                self.execution_times['simulation_treatments_cumulative'] = cumulative_time
                return False, 0, dict()

            self.execution_times['simulation_treatments_cumulative'] = cumulative_time
            self.execution_times['candidate_time'] = self.candidate_timer.stop()
            return True, init_time, {"tumour_diameter": behaviour}

        except RuntimeError:
            print("RuntimeError")
            return False, 0, dict()

    def reset_generation_object(self):
        self.model.instance.model_obj.resetToOrigin()
        self._restart_execution_times()
        self.reset_init_time()

    def simulate_for_initialisation(self, horizon) -> (bool, float, Dict[str, List[float]]):
        hor = horizon
        init_horizon = horizon
        init_time = -1
        can_be_init = False
        all_trajectories = dict()
        while hor >= init_horizon / 8:
            try:
                all_trajectories = self.sim_engine.simulate_trajectory(self.model, hor)
                init_time = math.floor(all_trajectories['initialization_tumour_diameter_monitor'][-1])

                if init_time > 0:
                    init_time = -1
                    if self.debug:
                        print("Candidate virtual patient doesn't show an appreciable tumour.")
                    return False, init_time, all_trajectories
                else:
                    can_be_init = True
                    if self.debug:
                        print("Candidate virtual patient can be initialised.")
                    break

            except Exception:
                can_be_init = False
                hor = hor - 100
                if self.debug:
                    print(f"trying again with horizon {hor}")

        if not can_be_init:
            return False, -1, dict()

        return True, init_time, all_trajectories

    def simulate_for_admissibility(self, horizon) -> (bool, Dict[str, List[float]]):
        try:
            all_trajectories = self.sim_engine.simulate_trajectory(self.model, horizon)
            is_sim = True

        except Exception:
            if self.debug:
                print("That candidate cannot be simulated, thus it is not admissible.")
            all_trajectories = dict()
            is_sim = False

        return is_sim, all_trajectories

    def _check_all_monitors_for_simulation(self, all_trajectories, treat_index: int):
        is_adm = True
        for monitor_name, trajectory in all_trajectories.items():
            if monitor_name not in ['initialization_tumour_diameter_monitor', 'time', 'tumour_diameter']:
                if trajectory[-1] > 0:
                    is_adm = False

        if not is_adm:
            if self.debug:
                print(f"The candidate is not admissible during the simulation of the treatment {treat_index}.")
            return False

        return True

    def _check_all_monitors_for_transient(self, all_trajectories, init_time):
        isadm = True
        for monitor_name, trajectory in all_trajectories.items():
            if monitor_name not in ['initialization_tumour_diameter_monitor', 'time', 'tumour_diameter']:
                if trajectory[:init_time + 1][-1] > 0:
                    isadm = False

        if not isadm:
            if self.debug:
                print("The candidate is not admissible during the transient.")
            return False
        return True

    def _restart_execution_times(self) -> None:
        self.execution_times['candidate_time'] = -1
        self.execution_times['simulation_initialisation'] = -1
        self.execution_times['simulation_treatments_cumulative'] = -1
        self.execution_times['simulation_no_treatment'] = -1
        self.execution_times['patient_initialisation'] = -1

    def patient_summary(self, pat_id: int) -> None:
        print(f"\n  Candidate {pat_id} Summary")
        print(
            f"  Candidate check transient simulation time: {self.execution_times['simulation_initialisation']:.2f} sec")
        print(f"  Candidate initialisation time: {self.execution_times['patient_initialisation']:.2f} sec")
        print(f"  Candidate no treatment simulation time: {self.execution_times['simulation_no_treatment']:.2f} sec")
        print(f"  Candidate all treatments-admissibility simulation cumulative "
              f"time: {self.execution_times['simulation_treatments_cumulative']:.2f} sec")
        print(f"  Candidate all generation time: {self.execution_times['candidate_time']:.2f} sec\n")

    def _set_ref_treatments(self, index: int):
        self.model.set_fixed_params(self.reference_treats[index])
