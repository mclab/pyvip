from apricopt.model.Model import Model
from apricopt.simulation.roadrunner.RoadRunnerEngine import RoadRunnerEngine
from apricopt.IO.data_input import get_conditions

import pandas as pd

from typing import List, Dict


def build_model(model_path: str, rel_tol: float, abs_tol: float, time_step: float,
                obserbed_outputs: List[str] = None):
    sim_engine = RoadRunnerEngine()
    model = Model(sim_engine=sim_engine, model_filename=model_path, rel_tol=rel_tol, abs_tol=abs_tol,
                  time_step=time_step, observed_outputs=obserbed_outputs)

    return model, sim_engine


def read_tsv(pathname: str) -> pd.DataFrame:
    frame = pd.read_csv(pathname, delimiter='\t')
    if 'parameterId' in frame.columns:
        frame.set_index('parameterId', inplace=True)
    elif 'conditionId' in frame.columns:
        frame.set_index('conditionId', inplace=True)
    elif 'observableId' in frame.columns:
        frame.set_index('observableId', inplace=True)
    return frame


def get_reference_treatments(treats_path: str):
    treatments_frame = read_tsv(treats_path)
    treatments = get_conditions(treatments_frame)
    return treatments


def get_virtual_patient(population_path: str, vp_row_index: int) -> Dict[str, float]:
    population_frame = pd.read_csv(population_path, delimiter='\t')
    patient_row = population_frame.iloc[vp_row_index]
    patient: Dict[str, float] = dict()
    for k, v in patient_row.items():
        patient[k] = v
    return patient
