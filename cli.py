# Author: Marco Esposito
# Last modified: 17/03/2022


import argparse

from crc.CRCAdmissibilityChecker import CRCAdmissibilityChecker


def get_command_line_arguments() -> dict:
    parser = argparse.ArgumentParser(description="Process Optimization output from SPET")

    parser.add_argument('-m', '--model', type=str,
                        help="The path to the model file",
                        required=True)

    parser.add_argument('-p', '--parameters', type=str,
                        help="The path to the parameters file",
                        required=True)

    parser.add_argument('-l', '--levels', type=str,
                        help="The path to the levels definitions file",
                        required=True)

    '''parser.add_argument('-y', '--phenotypes', type=str,
                        help="The path to the phenotypes definitions file",
                        required=True)'''

    parser.add_argument('-f', '--dump_filename', type=str,
                        help="The path of the TSV file where the population is dumped.",
                        required=True)

    parser.add_argument('--dump_failures_filename', type=str,
                        help="The path to the json file that stores the number of failures for each level.",
                        required=True)

    parser.add_argument('-g', '--given_vps_filename', type=str,
                        help="The path of the TSV file that contains a given (partial) population.",
                        required=False, default="")

    parser.add_argument('-e', '--epsilon', type=float,
                        help="The epsilon for MC^2. Default 0.05",
                        required=False, default=0.001)

    parser.add_argument('-d', '--delta', type=float,
                        help="The delta for MC^2. Default 0.05",
                        required=False, default=0.001)

    parser.add_argument('-E', '--policy_revision_epsilon', type=float,
                        help="The epsilon for MC^2 for sampling policy revision. Default 0.001",
                        required=False, default=0.001)

    parser.add_argument('-D', '--policy_revision_delta', type=float,
                        help="The delta for MC^2 for sampling policy revision. Default 0.05",
                        required=False, default=0.05)

    parser.add_argument('-c', '--n_cores', type=int,
                        help="The number of cores to use. Default 1",
                        required=False, default=1)

    parser.add_argument('-i', '--initial_failures_filename', type=str,
                        help="The path to the json file that contains the number of initial failures for each level."
                             "If not given, the number of initial failures is 0 for all levels",
                        required=False, default="")

    parser.add_argument('-n', '--samples_per_iter', type=int,
                        help="The number of candidate VPs to sample at each iteration. Default 1",
                        required=False, default=1)

    parser.add_argument('-s', '--seed', type=int,
                        help="The random seed. Default 1",
                        required=False, default=1)

    checker_class = CRCAdmissibilityChecker

    args = parser.parse_args()

    args_dict = {'model': args.model,
                 'parameters': args.parameters,
                 'levels_definitions': args.levels,
                 'dump_filename': args.dump_filename,
                 'dump_failures_filename': args.dump_failures_filename,
                 'given_vps_filename': args.given_vps_filename,
                 'epsilon': args.epsilon,
                 'delta': args.delta,
                 'policy_revision_delta': args.policy_revision_delta,
                 'policy_revision_epsilon': args.policy_revision_epsilon,
                 'n_cores': args.n_cores,
                 'samples_per_iter': args.samples_per_iter,
                 'checker_class': checker_class,
                 'initial_failures_filename': args.initial_failures_filename,
                 'seed': args.seed}

    print(f"Command line arguments:\n{args_dict}")
    return args_dict
