from abc import ABC, abstractmethod
from typing import *

import numpy as np


class AdmissibilityChecker(ABC):

    @abstractmethod
    def is_admissible(self, candidate: Dict[str, float], **kwargs) -> Tuple[bool, int, Dict[str, List[np.array]]]:
        pass
